import java.util.NoSuchElementException
import java.util.Scanner

fun main () {
    val scanner = Scanner(System.`in`)
    var students = mutableListOf<Estudiant>()
    do {
        println("""
        1. Consultar primer estudiant.
        2. Consultar últim estudiant.
        3. Afegir un estudiant.
        EXIT
        
        Escull una de les opcions
    """.trimIndent())
        val userSelection = scanner.next()
        when(userSelection){
            "1"->{
                try {
                    println(students.first())
                }catch (e: NoSuchElementException){
                    println("No hi han estudiants registrats")
                }
            }
            "2"->{
                try {
                    println(students.last())
                }catch (e: NoSuchElementException){
                    println("No hi han estudiants registrats")
                }
            }
            "3"->{
                println("Introdueix el nom")
                val nom = scanner.next()
                val nota = scanner.nextInt()
                try {
                    students.add(Estudiant(nom, nota))
                    println("Alumne afegit")
                }catch (e: UnsupportedOperationException){
                    println(" No s'ha pogut afegir de manera correcta")
                }

            }
            "4"-> students.removeAt(2)
            "EXIT"->{}
            else-> println("Escull una opció possible")
        }
    }while (true)
}