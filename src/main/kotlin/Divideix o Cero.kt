import java.lang.ArithmeticException
import java.util.Scanner

fun divideixoCero (dividend : Int, divisor  :Int) : Int {
    return try {
        dividend / divisor
    }catch (e: ArithmeticException){
        0
    }
}

fun main () {
    val scanner = Scanner(System.`in`)
    do {
        val dividend = scanner.nextInt()
        val divisor = scanner.nextInt()
        println(divideixoCero(dividend, divisor))
    }while (true)
}