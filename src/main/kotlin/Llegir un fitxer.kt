import java.io.File
import java.io.IOException

fun main (){
    try {
        File("src/main/kotlin/data.txt").readText()
    }catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida. ${e.message}")
    }
}