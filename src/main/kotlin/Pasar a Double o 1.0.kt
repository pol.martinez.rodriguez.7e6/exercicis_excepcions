import java.lang.NumberFormatException
import java.util.Scanner

fun aDoubleoU (number :String): Double {
    return try {
        number.toDouble()
    }catch (e: NumberFormatException){
        1.0
    }
}

fun main () {
    val scanner = Scanner(System.`in`)
    do {
        println(aDoubleoU(scanner.next()))
    }while (true)
}